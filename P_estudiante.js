function cargarPracticasEstudiante() {
    const tablaPracticas = document.querySelector('table tbody');

    const storedPracticas = JSON.parse(localStorage.getItem('practicas'));

    if (storedPracticas) {
        // Limpia la tabla antes de agregar las prácticas
        tablaPracticas.innerHTML = '';

        storedPracticas.forEach(practica => {
            const fila = document.createElement('tr');

            const celdas = [
                practica.alumno,
                practica.empresa,
                practica.estado,
                practica.duracion,
                practica.encargado,
                practica.contacto,
                practica.calificacion
            ];

            celdas.forEach(texto => {
                const celda = document.createElement('td');
                celda.textContent = texto;
                fila.appendChild(celda);
            });

            tablaPracticas.appendChild(fila);
        });
    }
}
document.addEventListener('DOMContentLoaded', function() {
    const storedPracticas = JSON.parse(localStorage.getItem('practicas'));
    
    if (storedPracticas) {
        const calificacion = storedPracticas[0].calificacion; // Suponiendo que quieres la calificación de la primera práctica
        
        // Haz algo con la calificación, por ejemplo, mostrarla en un elemento en el HTML
        const elementoCalificacion = document.getElementById('calificacion');
        elementoCalificacion.textContent = calificacion;
    }
});

window.onload = cargarPracticasEstudiante;
