function mostrarAlerta(mensaje) {
    alert(mensaje);
}

function cargarPracticasEstudiante() {
    const tablaPracticas = document.querySelector('table tbody');

    const storedPracticas = JSON.parse(localStorage.getItem('practicas'));

    if (storedPracticas) {
        tablaPracticas.innerHTML = '';

        storedPracticas.forEach((practica, index) => {
            const fila = document.createElement('tr');

            const celdas = [
                practica.alumno,
                practica.empresa,
                practica.estado,
                practica.duracion,
                practica.encargado,
                practica.contacto
            ];

            celdas.forEach(texto => {
                const celda = document.createElement('td');
                celda.textContent = texto;
                fila.appendChild(celda);
            });

            // Agrega el campo de calificación
            const inputCalificacion = document.createElement('input');
            inputCalificacion.type = 'number';
            inputCalificacion.min = 0;
            inputCalificacion.max = 10;
            inputCalificacion.value = practica.calificacion || ''; // Si existe una calificación previa, mostrarla
            inputCalificacion.id = `calificacion${index}`; // Asigna un ID único para identificarlo más tarde

            const celdaCalificacion = document.createElement('td');
            celdaCalificacion.appendChild(inputCalificacion);
            fila.appendChild(celdaCalificacion);


              // Agrega un campo de texto para mostrar la calificación
              const textoCalificacion = document.createElement('td');
              textoCalificacion.textContent = practica.calificacion || ''; // Si no hay calificación, dejarlo vacío
              fila.appendChild(textoCalificacion);
  
              tablaPracticas.appendChild(fila);
        });
    }
}
function guardarCalificacion(index) {
    const calificacion = document.querySelector(`#calificacion${index}`).value;
    const storedPracticas = JSON.parse(localStorage.getItem('practicas'));

    if (storedPracticas) {
        storedPracticas[index].calificacion = calificacion;
        localStorage.setItem('practicas', JSON.stringify(storedPracticas));
        
        mostrarAlerta('Calificación guardada exitosamente');
    }
}

window.onload = cargarPracticasEstudiante;