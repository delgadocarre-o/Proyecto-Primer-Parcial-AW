
const practicasAsignadas = [];


function cargarPracticas() {
    const listaPracticas = document.getElementById('lista-practicas');
    const storedPracticas = JSON.parse(localStorage.getItem('practicas'));

    if (storedPracticas) {
        practicasAsignadas.push(...storedPracticas);
    }

    practicasAsignadas.forEach(practica => {
        const fila = document.createElement('tr');
        fila.dataset.nombreEstudiante = practica.alumno;

        const celdas = [
            practica.alumno,
            practica.duracion,
            practica.duracion2,
            practica.empresa,
            practica.encargado,
            practica.contacto,
            '<button onclick="eliminarPractica(event)">Eliminar</button>'
        ];

        celdas.forEach(texto => {
            const celda = document.createElement('td');
            celda.innerHTML = texto;
            fila.appendChild(celda);
        });

        listaPracticas.appendChild(fila);
    });
}
function agregarPractica(event) {
    event.preventDefault();

    const nombreEstudiante = document.getElementById('nombre-estudiante').value;
    const duracion = document.getElementById('duracion').value.split('/').reverse().join('-');
    const duracion2 = document.getElementById('duracion2').value.split('/').reverse().join('-');
    const empresa = document.getElementById('empresa').value;
    const encargado = document.getElementById('encargado').value;
    const contacto = document.getElementById('contacto').value;


    // Validar que ninguno de los campos esté vacío
    if (nombreEstudiante && duracion && duracion2 && empresa && encargado && contacto) {
        practicasAsignadas.push({
            alumno: nombreEstudiante,
            empresa: empresa,
            estado: "En Progreso",
            duracion: duracion,
            duracion2: duracion2,
            encargado: encargado,
            contacto: contacto
        });

        const listaPracticas = document.getElementById('lista-practicas');
        const fila = document.createElement('tr');

        fila.innerHTML = `
            <td> ${ nombreEstudiante } </td>
            <td> ${ duracion } </td>
            <td> ${ duracion2 } </td>
            <td> ${ empresa } </td>
            <td> ${ encargado } </td>
            <td> ${ contacto } </td>
            <td>
                <button onclick="eliminarPractica(event)">Eliminar</button>
            </td>
        `;

        listaPracticas.appendChild(fila);

        document.getElementById('nombre-estudiante').value = '';
        document.getElementById('duracion').value = '';
        document.getElementById('duracion2').value = '';
        document.getElementById('empresa').value = '';
        document.getElementById('encargado').value = '';
        document.getElementById('contacto').value = '';

        if (practicasAsignadas.length === 1) {
            document.getElementById('tabla-practicas').querySelector('thead').style.display = 'table-header-group';
        }

        if (practicasAsignadas.length === 0) {
            document.getElementById('tabla-practicas').querySelector('thead').style.display = 'none';
        }

        localStorage.setItem('practicas', JSON.stringify(practicasAsignadas));        


    } else {
        alert('Por favor completa todos los campos.');
    }

}


function eliminarPractica(event) {
    const botonEliminar = event.target;
    const fila = botonEliminar.parentElement.parentElement;
    const nombreEstudiante = fila.dataset.nombreEstudiante;

    practicasAsignadas.forEach((practica, index) => {
        if (practica.alumno === nombreEstudiante) {
            practicasAsignadas.splice(index, 1);
        }
    });

    fila.remove();

    localStorage.setItem('practicas', JSON.stringify(practicasAsignadas));
}

function retrocederAInicio() {
    window.location.href = 'inicio_docente.html';
}

function cerrarSesion() {
    window.location.href = 'home.html';
}

window.onload = cargarPracticas;
